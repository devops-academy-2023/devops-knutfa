import React from "react";
import { render } from "react-dom";
import { App } from "../app";

describe("application", () => {
  it("shows application front page", () => {
    const element = document.createElement("div");
    render(<App />, element);
    expect(element.innerHTML).toMatchSnapshot();
  });
});
